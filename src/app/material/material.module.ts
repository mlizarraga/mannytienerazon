import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { MatSliderModule } from '@angular/material/slider';
import {MatSidenavModule} from '@angular/material/sidenav';

import {MatTableModule} from '@angular/material/table';
const materialComponents = [
  MatSliderModule,
  MatSidenavModule,
  MatTableModule
];

@NgModule({
  imports: [
    CommonModule,
    MatSliderModule,
  MatSidenavModule,
  MatTableModule
  ],
  exports: [ MatSliderModule,
    MatSidenavModule,
    MatTableModule]
})
export class MaterialModule { }
