import { Routes } from '@angular/router';
import { AppComponent } from '../app/app.component';
import { ColorsComponent } from '../app/components/colors/colors.component';
import { GroupsComponent } from '../app/components/groups/groups.component';
import { BrandsComponent } from '../app/components/brands/brands.component';
import { NotificacionsComponent } from '../app/components/notificacions/notificacions.component';

// tslint:disable-next-line: variable-name
export const _Routes: Routes= [
    { path:'' , component:NotificacionsComponent } ,
    { path: 'colors' , component: ColorsComponent},
    { path: 'groups' , component: GroupsComponent},
    { path: 'brands' , component: BrandsComponent},
    { path: '**' , component: NotificacionsComponent}
];
